import os
import pandas as pd
import numpy as np

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage

from io import StringIO

import string
from re import sub
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from gensim import corpora
from collections import defaultdict

from gensim.models.wrappers import DtmModel

import gensim
import pyLDAvis
import matplotlib.pyplot as plt
import math


def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()
    codec = 'utf-8'
    laparams = LAParams()
    laparams.all_texts = True
    device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    password = ""
    caching = True
    pagenos = set()

    for page in PDFPage.get_pages(fp, pagenos, password=password, caching=caching, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text


def extract_articles_to_txt(input_path="ressources/articles/", output_path="articles_txt"):
    if not os.path.isdir(output_path):
        os.mkdir(output_path)

    for d in os.listdir(input_path):
        if not os.path.isdir(output_path + d):
            os.mkdir(output_path + "/" + d)

        for filename in os.listdir(input_path + d):
            try:
                text_content = convert_pdf_to_txt(input_path + d + "/" + filename)
                print("Processing " + d + "/" + filename + " : " + str(len(text_content)) + " characters")

                if text_content.strip():
                    with open(output_path + "/" + d + "/" + (filename[:-4]) + ".txt", "w+", encoding="utf-8") as f:
                        f.write(text_content)
                        f.flush()
                        os.fsync(f.fileno())

            except Exception:
                pass


def add_article_paths(df):
    if not os.path.isdir("articles_txt"):
        os.mkdir("articles_txt")
        extract_articles_to_txt()

    articles_paths = df['year'].apply(lambda x: 'articles_txt/EGC' + str(x) + '/')
    df['txtarticle'] = df['pdfarticle'].apply(lambda x: x[-7:] + '.txt')
    df['txtarticle'] = articles_paths + df['txtarticle']


def add_references_paths(df):
    if not os.path.isdir("references_txt"):
        os.mkdir("references_txt")
        # @TODO: create Python3 compatible get_references.py

    references_paths = df['year'].apply(lambda x: 'references_txt/EGC' + str(x) + '/')
    df['txtreferences'] = df['pdfarticle'].apply(lambda x: x[-7:] + '.txt')
    df['txtreferences'] = references_paths + df['txtreferences']


def structure_data(csv_path="ressources/export_articles.csv"):
    print("Get CSV...")
    df = pd.read_csv(csv_path, sep=";", error_bad_lines=False)
    print("Done. Adding articles paths to df...")
    add_article_paths(df)
    print("Done. Adding references paths to df...")
    add_references_paths(df)
    print("Done. Saving csv...")
    df.to_csv("structured_articles.csv")
    print("Done. Head(10) : ")
    print(df.head(10))
    return df


def preprocessing(data):
    text = sub(r'\d+', '', str(data).lower().strip())
    if text == "nan":
        return False
    text = word_tokenize(text.translate(str.maketrans('', '', string.punctuation)))
    stop_words = set(stopwords.words('english'))

    text = [e for e in text if e not in stop_words and len(e) >= 3]
    stemmer = SnowballStemmer('english')
    text = [stemmer.stem(e) for e in text]
    return text


def get_dtm_model(name="all_abstract", n_topics=5, only_abstract=True, articles_path="articles_txt"):
    if not os.path.isfile(name + "_" + str(n_topics) + ".model"):
        df = structure_data()
        texts = []

        if only_abstract:
            abstracts = [e for e in df['abstract_en']]
            for content in abstracts:
                text = preprocessing(content)
                if text:
                    texts.append(text)

            texts = remove_rare_words(texts)

            corpus, dictionary = get_corpus_and_dict(texts)
            time_slices = [df[df.year == e].count().abstract for e in range(2004, 2019)]
        else:
            time_slices=[]
            for dirname in os.listdir(articles_path):
                file_count = 0
                for filename in os.listdir(articles_path + "/" + dirname):
                    with open(articles_path + "/" + dirname + "/" + filename, "r+", encoding="utf-8") as f:
                        print("Using file " + filename + " from " + dirname)
                        text = preprocessing(f.read())
                        if text:
                            texts.append(text)
                            file_count += 1

                texts = remove_rare_words(texts)
                time_slices.append(file_count)

            corpus, dictionary = get_corpus_and_dict(texts)

        path_to_dtm_binary = "dtm-win64.exe"
        model = DtmModel(
            path_to_dtm_binary,
            corpus=corpus[-sum(time_slices):],
            id2word=dictionary,
            time_slices=time_slices,
            num_topics=n_topics
        )

        model.save(name + "_" + str(n_topics) + ".model")
        return model

    else:
        return gensim.utils.SaveLoad.load(name + "_" + str(n_topics) + ".model")


def get_corpus_and_dict(texts):
    dictionary = corpora.Dictionary(texts)
    return [dictionary.doc2bow(text) for text in texts], dictionary


def remove_rare_words(texts):
    frequency = defaultdict(int)

    print("Counting frequencies ")
    for text in texts:
        for token in text:
            frequency[token] += 1

    print("Removing rare words ")
    texts = [
        [token for token in text if frequency[token] > 3]
        for text in texts
    ]

    return texts


def get_all_abstract_params():
    df = structure_data()
    abstracts = [e for e in df['abstract_en']]
    texts = []

    for content in abstracts:
        text = preprocessing(content)
        if text:
            texts.append(text)

    texts = remove_rare_words(texts)
    corpus, dictionary = get_corpus_and_dict(texts)
    time_slices = [df[df.year == e].count().abstract for e in range(2004, 2019)]

    return dictionary, corpus, time_slices


def get_all_articles_params(input_path="articles_txt"):
    texts = []
    time_slices = []
    for dirname in os.listdir(input_path):
        file_count = 0
        for filename in os.listdir(input_path + "/" + dirname):
            with open(input_path + "/" + dirname + "/" + filename, "r+", encoding="utf-8") as f:
                print("Using file " + filename + " from " + dirname)
                text = preprocessing(f.read())
                if text:
                    texts.append(text)
                    file_count += 1

        texts = remove_rare_words(texts)
        time_slices.append(file_count)

    corpus, dictionary = get_corpus_and_dict(texts)

    return dictionary, corpus, time_slices


def print_ldavis(model, time=0):
    print("Done. Getting all params from abstracts...")
    corpus = get_all_abstract_params()[1]

    # dictionary, corpus, time_slices = get_all_articles_params()
    print("Done. Getting data specified by LDAvis format...")
    doc_topic, topic_term, doc_lengths, term_frequency, vocab = model.dtm_vis(time=time, corpus=corpus)
    print("Done. Preparing data...")
    vis_wrapper = pyLDAvis.prepare(topic_term_dists=topic_term, doc_topic_dists=doc_topic, doc_lengths=doc_lengths,
                                   vocab=vocab, term_frequency=term_frequency)
    print("Done.")
    pyLDAvis.show(vis_wrapper)


def closest_node(data, t, map, m_rows, m_cols):
    # (row,col) of map node closest to data[t]
    result = (0, 0)
    small_dist = 1.0e20
    for i in range(m_rows):
        for j in range(m_cols):
            ed = euc_dist(map[i][j], data[t])
            if ed < small_dist:
                small_dist = ed
                result = (i, j)
    return result


def euc_dist(v1, v2):
    return np.linalg.norm(v1 - v2)


def manhattan_dist(r1, c1, r2, c2):
    return np.abs(r1 - r2) + np.abs(c1 - c2)


def most_common(lst):
    if not lst:
        return -1
    return np.argmax(lst)


def som_on_documents(dictionary,corpus,epochs=1000, max_learn=0.5):

    init_dict = [0 for _ in dictionary]

    my_corpus = []
    for d in corpus:
        tmp_dict = init_dict.copy()
        for w in d:
            tmp_dict[w[0]] = w[1]
        my_corpus.append(tmp_dict)

    my_corpus = np.asarray(my_corpus)

    np.random.seed(1)
    dim = len(init_dict)

    n_rows = 8
    n_cols = 8

    max_range = n_rows + n_cols

    map = np.random.random_sample(size=(n_rows, n_cols, dim))
    for s in range(epochs):
        if s % 100 == 0:
            print("step ", str(s), "/", str(epochs))
        pct_left = 1.0 - ((s * 1.0) / epochs)
        curr_range = (int)(pct_left * max_range)
        curr_rate = pct_left * max_learn

        t = np.random.randint(len(my_corpus))
        (bmu_row, bmu_col) = closest_node(my_corpus, t, map, n_rows, n_cols)
        for i in range(n_rows):
            for j in range(n_cols):
                if manhattan_dist(bmu_row, bmu_col, i, j) < curr_range:
                    map[i][j] = map[i][j] + curr_rate * my_corpus[t] - map[i][j]

    return map, my_corpus


def som_on_document_by_year(year, epochs=1000):
    print("Done. Getting all params from abstracts...")
    dictionary, corpus, time_slices = get_all_abstract_params()
    id_ts=year-2004
    year_corpus = corpus[sum(time_slices[0:id_ts]):(sum(time_slices[0:id_ts])+time_slices[id_ts])]
    print(len(year_corpus))
    map,corpus=som_on_documents(dictionary,year_corpus,epochs)
    print("SOM construction complete \n")

    n_rows = map.shape[0]
    n_cols = map.shape[1]

    # 3. construct U-Matrix
    print("Constructing U-Matrix from SOM")
    u_matrix = np.zeros(shape=(n_rows, n_cols), dtype=np.float64)
    for i in range(n_rows):
        for j in range(n_cols):
            v = map[i][j]  # a vector
            sum_dists = 0.0
            ct = 0

            if i - 1 >= 0:  # above
                sum_dists += euc_dist(v, map[i - 1][j])
                ct += 1
            if i + 1 <= n_rows - 1:  # below
                sum_dists += euc_dist(v, map[i + 1][j])
                ct += 1
            if j - 1 >= 0:  # left
                sum_dists += euc_dist(v, map[i][j - 1])
                ct += 1
            if j + 1 <= n_cols - 1:  # right
                sum_dists += euc_dist(v, map[i][j + 1])
                ct += 1

            u_matrix[i][j] = sum_dists / ct
    print("U-Matrix constructed \n")

    # display U-Matrix
    plt.imshow(u_matrix, cmap='gray')  # black = close = clusters
    plt.show()


    # 4. because the data has labels, another possible visualization:
    # associate each data label with a map node
    print("Associating each data label to one map node ")
    mapping = np.empty(shape=(n_rows, n_cols), dtype=object)
    for i in range(n_rows):
        for j in range(n_cols):
            mapping[i][j] = []

    for t in range(len(corpus)):
        (m_row, m_col) = closest_node(corpus, t, map, n_rows, n_cols)
        mapping[m_row][m_col].append(dictionary[t])

    label_map = np.zeros(shape=(n_rows, n_cols), dtype=int)
    for i in range(n_rows):
        for j in range(n_cols):
            label = most_common(mapping[i][j])
            if label != -1:
                label_map[i][j] = mapping[i][j][label]
            else:
                label_map[i][j] = "not_assigned"

    plt.imshow(label_map)
    plt.colorbar()
    plt.show()

som_on_document_by_year(2004,epochs=10000)
