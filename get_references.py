# Need Python 2.7
import os
import codecs
from refextract import extract_references_from_file

def extract_references_to_txt(input_path = "ressources/articles/",output_path = "references_txt/"):
    if not os.path.isdir(output_path):
        os.mkdir(output_path[:-1])
    for d in os.listdir(input_path):
        if not os.path.isdir(output_path+d):
            os.mkdir(output_path+d)
        for filename in os.listdir(input_path+d):
            try:
                text_content = extract_references_from_file(input_path + d + "/" + filename)
                print("Processing " + d + "/" + filename)
                with codecs.open(output_path+d+"/"+(filename[:-4])+".txt", "w+", encoding="utf-8") as f:
                    f.write('\n'.join(map(str, text_content)))
            except Exception:
                pass

#extract_references_to_txt()

