from os import path

from gensim.models.wrappers.dtmmodel import DtmModel
from gensim.utils import SaveLoad

from pickle import load as pickle_load

from pyLDAvis import prepare as dtm_vis_prepare
from pyLDAvis import show as dtm_vis_show
from pyLDAvis import save_html

from pprint import pprint

from numpy import argmax

from matplotlib.pyplot import figure
from matplotlib.pyplot import subplot
from matplotlib.pyplot import show as plt_show


def get_params(param_path="ressources/"):
    dictionary = SaveLoad.load(param_path + "dictionary.bin")

    with open(param_path + "word_count.bin", "rb") as f:
        corpus = pickle_load(f)

    with open(param_path + "time_slices.bin", "rb") as f:
        time_slices = pickle_load(f)

    return dictionary, corpus, time_slices


def get_dtm_model(n_topics=5, filename="egc.dtmmodel", param_path="ressources/", engine_path="ressources/dtm-win64.exe"):
    if path.isfile(filename):
        return SaveLoad.load(filename)

    dictionary, corpus, time_slices = get_params(param_path)

    path_to_dtm_binary = engine_path
    model = DtmModel(
        path_to_dtm_binary,
        corpus=corpus[-sum(time_slices):],
        id2word=dictionary,
        time_slices=time_slices,
        num_topics=n_topics
    )

    model.save(filename)
    return model


def print_dtm_model(model, output_path, timeslice=0):
    print("Getting params.")
    dictionary, corpus, time_slices = get_params()
    print("Done. Preparing model")
    doc_topic, topic_term, doc_lengths, term_frequency, vocab = model.dtm_vis(time=timeslice, corpus=corpus)
    vis_wrapper = dtm_vis_prepare(topic_term_dists=topic_term, doc_topic_dists=doc_topic, doc_lengths=doc_lengths,
                                  vocab=vocab, term_frequency=term_frequency)
    print("Done. Saving model")
    save_html(vis_wrapper, output_path)
    print("Done.")
    #dtm_vis_show(vis_wrapper)


def show_evolution(n_topics, topics_id):
    dictionary, corpus, time_slices = get_params()

    doc_topic, topic_term, doc_lengths, term_frequency, vocab = model.dtm_vis(time=0, corpus=corpus)
    doc_topics = [argmax(e) if max(e) != 1.0 / n_topics else -1 for e in doc_topic]

    sliced_doc_topics = list()
    current = 0
    for time_slice in time_slices:
        sliced_doc_topics.append([doc_topics[current:current + time_slice] for time_slice in time_slices][0])
        current += time_slice
    sliced_topic_freq = [[e.count(k) + e.count(-1) for k in range(n_topics)] for e in sliced_doc_topics]

    coord_topics = []
    for k in range(n_topics):
        x = []
        y = []
        for i, year_freq in enumerate(sliced_topic_freq):
            x.append(i)
            y.append(year_freq[k] * 100 / (sum(year_freq)))
        coord_topics.append([x, y])

    fig = figure()
    ax = subplot(111)

    if not topics_id:
        for i in range(n_topics):
            ax.plot(coord_topics[i][0], coord_topics[i][1])
        ax.legend(["Topic " + str(i) for i in range(n_topics)])
        plt_show()
    else:
        for e in topics_id:
            ax.plot(coord_topics[e][0], coord_topics[e][1])
        ax.legend(["Topic " + str(e) for e in topics_id])
        plt_show()


model = get_dtm_model(filename="abstract_egc_15.dtmmodel", n_topics=15)
show_evolution(n_topics=15, topics_id=[4,12])
#print_dtm_model(model, timeslice=7, output_path="output/test_time_7_5.html")
#print_dtm_model(model, timeslice=14, output_path="output/test_time_14.html")
