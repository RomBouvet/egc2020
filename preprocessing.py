from os import path
from os import remove

from string import punctuation
from re import sub

from spacy import load as spacy_load

from pandas import read_csv

from gensim import corpora
from gensim.utils import SaveLoad

from pickle import dump
from pickle import load as pickle_load

'''from nltk.stem.snowball import SnowballStemmer

stemmer = SnowballStemmer('french')'''


def preprocess(data, sp):
    # lowercase
    text = str(data).lower()
    # remove numbers
    text = sub(r'\d+', '', text)
    # remove punctuation
    text = text.translate(str.maketrans(punctuation, ' '*len(punctuation)))
    # remove redundant spaces
    text = text.strip()
    # remove new lines
    text = text.rstrip()

    print("preprocessing " + text[0:10])

    # creating spacy doc
    doc = sp(text)
    # tokenization + lemmatization + remove stop words
    tokens = [token.lemma_ for token in doc if not token.is_stop and len(token) > 4]
    '''tokens = [token.text for token in doc if not token.is_stop and len(token) > 4]
    tokens = [stemmer.stem(token) for token in tokens]'''
    return tokens


def create_dictionary(articles_tokens):
    return corpora.Dictionary(articles_tokens)


def preprocess_all_articles(df, abstract_only=True, output_path="ressources/"):
    # load spacy model
    sp = spacy_load('fr')
    # preprocess each article in a list
    articles_tokens = [preprocess(e, sp) for e in df['abstract' if abstract_only else 'fullarticle']]
    # create dictionary from the articles tokens
    dictionary = create_dictionary(articles_tokens)
    # create word_count from dictionary
    word_count = [dictionary.doc2bow(tokens) for tokens in articles_tokens]
    # define time slices
    time_slices = [df[df['year'] == e].shape[0] for e in df['year'].unique()]

    # saving dictionary to file
    dictionary.save(output_path + "dictionary.bin")

    # saving word count to file
    if path.isfile(output_path + "word_count.bin"):
        remove(output_path + "word_count.bin")
    with open(output_path + "word_count.bin", "wb") as f:
        dump(word_count, f)

    # saving time slices to file
    if path.isfile(output_path + "time_slices.bin"):
        remove(output_path + "time_slices.bin")
    with open(output_path + "time_slices.bin", "wb") as f:
        dump(time_slices, f)


import time
start_time = time.time()

df = read_csv("structured_articles.csv")
preprocess_all_articles(df, abstract_only=True)

dictionary = SaveLoad.load("ressources/dictionary.bin")
print("Dictionary : \n", dictionary)

with open("ressources/time_slices.bin", "rb") as f:
    time_slices = pickle_load(f)
print("Time slices : \n", time_slices)

with open("ressources/word_count.bin", "rb") as f:
    word_count = pickle_load(f)
print("Word count: \n", word_count)
print("--- %s seconds ---" % (time.time() - start_time))