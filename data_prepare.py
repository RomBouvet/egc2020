from os import path
from os import mkdir
from os import remove

from pandas import read_csv

from urllib import request

from langdetect import detect

from tika import initVM
from tika import parser


initVM()


def download_article(row, output_path, max_retries=10):
    print(row['year'], "- downloading n", row['pdfarticle'][-7:], " : ", row['title'])
    articles_path = output_path + "/" + str(row['year']) + "/" + row['pdfarticle'][-7:] + ".txt"

    if path.isfile(articles_path):
        return

    while max_retries > 0:
        try:
            request.urlretrieve(row['pdfarticle'], '_tmp.pdf')
            text = parser.from_file("_tmp.pdf")["content"]
            if detect(text) != 'fr':
                return ''

            text = text.strip('\t').strip('\n')

            with open(articles_path, "w", encoding="utf-8") as f:
                f.write(text)
            max_retries = 0
        except (TypeError, FileNotFoundError):
            max_retries = 0
            pass
        except request.http.client.RemoteDisconnected:
            max_retries -= 1
            pass
    return


def download_all_articles(csv_path="ressources/export_articles.csv", output_path="ressources/articles_txt", max_retries=10):
    print("Get CSV...")
    df = read_csv(csv_path, sep=";", error_bad_lines=False)

    try:
        mkdir(output_path)
    except FileExistsError:
        pass

    for year in df['year'].unique():
        try:
            mkdir(output_path + "/" + str(year))
        except FileExistsError:
            pass

    df.apply(lambda row: download_article(row, output_path, max_retries), axis=1)

    try:
        remove("_tmp.pdf")
    except FileNotFoundError:
        pass


def add_article(row, articles_path):
    print(str(row['year']), " - extracting text from '", row['title'], "'")
    try:
        article_path = articles_path + "/" + str(row['year']) + "/" + str(row['pdfarticle'][-7:]) + ".txt"
        with open(article_path, "r",
                  encoding="utf-8") as f:
            return f.read()
    except FileNotFoundError as e:
        return ''


def add_articles(df, articles_path="ressources/articles_txt"):
    df['fullarticle'] = df.apply(lambda row: add_article(row, articles_path), axis=1)
    return df


def translate_abstract(row):
    text = str(row['abstract'])
    if text == '' or detect(text) != 'fr':
        return ''
    return text


def translate_abstracts(df):
    df['abstract'] = df.apply(lambda row: translate_abstract(row), axis=1)
    return df


def structure_data(csv_path="ressources/export_articles.csv"):
    print("Get CSV...")
    df = read_csv(csv_path, sep=";", error_bad_lines=False)
    print("Done. Adding articles paths to df...")
    df = add_articles(df)
    print("Done. Translating abtracts in french")
    #df = translate_abstracts(df)
    # @TODO: Add with refextract Python3
    #print("Done. Adding references paths to df...")
    #df = add_references(df)
    df.sort_values(by='year', ascending=True).to_csv("structured_articles.csv")
    print("Done. Head(10) : ")
    print(df.head(10))
    return df


download_all_articles()
structure_data()